/* eslint-disable eqeqeq */
/* eslint-disable no-throw-literal */
/* +++++++ DECLARATION DES CONSTANTES GLOBALES +++++++ */
'use strict'
const nbPair = document.getElementById('cartes')
const jeu = document.getElementById('jeu')
const boutonSoumettre = document.getElementById('soumettre')
const timer = document.getElementById('minuterie')
const regex = /^\w+$/
let nomUtilisateur =
  boutonSoumettre.addEventListener('click', validationPaires)

/**
* Cette partie du formulaire valid d'abord les paires,
* si le nombre entree est valide, procede a la validation du nom.
* @param {string} message le message d'erreur ou nom du joueur si aucune
* erreur n'est trouver.
*/

function validationPaires () {
  let x
  const message = document.getElementById('erreur')
  message.innerHTML = ''
  x = document.getElementById('cartes').value
  try {
    if (x == '') throw 'est vide'
    if (isNaN(x)) throw "n'est pas un nombre"
    x = Number(x)
    if (x < 2) throw 'est trop basse'
    if (x > 10) throw 'est trop haute'
    if (x > 2 || x < 10) {
      validationNom()
    }
  } catch (err) {
    message.innerHTML = 'Votre paire entrée ' + err
  }
}
/**
* Partie de la validation du formulaire, si les parametre son correcte
* procede au Jeu de Memoire.
* @param {boolean} resultat le resultat en boolean
*
*/
function validationNom () {
  nomUtilisateur = document.getElementById('nom').value
  const message = document.getElementById('erreur')
  const resultat = regex.test(nomUtilisateur)
  if (resultat === true) {
    message.innerHTML = 'Bonjour ' + nomUtilisateur
    jeuMemoire()
    minuterie()
    boutonSoumettre.setAttribute('disabled', true)
  } else if (resultat === false) {
    message.innerHTML = 'Vous devez entrer un nom alphanumérique'
  }
}
/**
 * Function de la minuterie, si vous changer la const de minutes.
 * Vous changerez directement votre temps d'essai.
 * ------------------------------------------------------
 * Note pour Phillippe : Lors du countdown de la minuterie,
 * mon script saute une seconde a partir de 10 secondes,
 *  mais l'ajoute ensuite en comptant 2 fois les 0 secondes.
 * Donc le temps est le meme
 * Comment pourrais-je proceder pour corriger ce petit glitch visuel sans trop changer mon code?
 * -------------------------------------------------------
 */
function minuterie () {
  const minutes = 1
  let secondes = (minutes * 60)
  let minutesRestantes = 0
  const interval = setInterval(function () {
    minutesRestantes = Math.floor(secondes / 60)
    timer.innerHTML = minutesRestantes + ':' + secondes % 60
    secondes = secondes - 1
    console.log(secondes)
    if ((secondes % 60) < 10) {
      timer.innerHTML = minutesRestantes + ':' + '0' + secondes % 60
    }
    if (secondes === 0) {
      clearInterval(interval)
      alert('Votre temps est écoulé! Veuillez réessayer.')
      jeu.innerHTML = ''
      boutonSoumettre.removeAttribute('disabled')
    }
  }, 1000)
}

/* +++++++ DEBUT DU JEU DE MEMOIRE +++++++ */
function jeuMemoire () {
  const nbPaires = nbPair.value
  const tableauCartes = []
  for (let i = 0; i < nbPaires; i++) {
    tableauCartes.push(i)
    tableauCartes.push(i)
  }

  /*
  * Cette partie de la function permet de melanger les cartes
  * avec un algorithme math.random multiplier
  * par la longeur du tableau
  */

  const tableauCartesMelangees = []
  while (tableauCartes.length > 0) {
    const index = Math.floor(Math.random() * tableauCartes.length)
    tableauCartesMelangees.push(tableauCartes[index])
    tableauCartes.splice(index, 1)
  }

  /*
  * cette partie cree les carte selon deux parametre.
  * La partie qui sera cacher  '?' et la valeur de la carte
  */

  for (let i = 0; i < tableauCartesMelangees.length; i++) {
    const idCarte = tableauCartesMelangees[i]
    const boutonCarte = document.createElement('bouton')
    const carteCacher = document.createTextNode('?')
    boutonCarte.classList.add('display')
    boutonCarte.setAttribute('data-nombre-cache', idCarte)
    boutonCarte.appendChild(carteCacher)
    jeu.appendChild(boutonCarte)
    boutonCarte.addEventListener('click', tournerCarte)
  }

  /**
   *  Cette function permet de tourner
  * les cartes et compare les deux cartes
  * si le contenu des cartes est different
  * les mets en disabled.
  * ---------------------------------------------------
  * Notes pour Phillipe: Il est possible de clicker rapidement sur plusieurs cartes en meme temps
  */

  let cartesTourner = []
  let pairesTrouver = 0

  function tournerCarte (e) {
    const idCarte = e.target.getAttribute('data-nombre-cache')
    cartesTourner.push(e.target)
    cartesTourner[0].setAttribute('disabled', true)
    e.target.textContent = idCarte
    if (cartesTourner.length === 2) {
      const carte1 = cartesTourner[0]
      const carte2 = cartesTourner[1]
      carte2.setAttribute('disabled', true)
      if (carte1.textContent === carte2.textContent) {
        pairesTrouver = pairesTrouver + 1
        cartesTourner[0].setAttribute('disabled', true)
        cartesTourner[1].setAttribute('disabled', true)
        cartesTourner = []
      } else {
        setTimeout(hideCards, 1000)
      }

      /**
       * * Si la paire n'est pas identique,
      * retourne la function hideCards() apres 1 seconde.
      */

      function hideCards () {
        carte1.textContent = '?'
        carte2.textContent = '?'
        carte1.removeAttribute('disabled')
        carte2.removeAttribute('disabled')
      }
      cartesTourner = []
    }
    if (pairesTrouver == nbPaires) {
      alert('Bravo, vous avez trouver toutes les paires !')
      jeu.innerHTML = ''
      boutonSoumettre.removeAttribute('disabled')
    }
  }
}
